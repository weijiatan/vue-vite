import vue from '@vitejs/plugin-vue'
import copy from 'rollup-plugin-copy'
const path = require('path')

export default{
    plugins: [vue(),copy({
        targets: [
          { src: './public/index.html', dest: './Index' },
        ]
      })],
    base: './',
    resolve:{
        alias:{
            '@': path.resolve(__dirname, './src')
        }
    },
    server:{
        proxy: {
            '/api': {
                target: 'http://platform',
                changeOrigin: true,
                rewrite: path => path.replace(/^\/api/, '')
            }
        }
    },
    // 生产环境移除console
    build:{
        terserOptions:{
            compress:{
                drop_console:true
            }
        },
        outDir:'public',   //指定输出路径
        assetsDir: "wap", //指定生成静态资源的存放路径
    },
}